# Connecting USB devices to WSL


## PowerShell
Listar dispositivos conectados via USB
```powershell
usbipd list
```

Habilitar compartilhamento de dispositivo

```powershell
(Admin)> usbipd bind --busid 3-1
```

Vincular e desvincular dispositivo ao WSL
```powershell
usbipd attach --wsl --busid 3-1
usbipd detach --busid 3-1
```

## Bash
```bash
ls /dev/tty*
```


## udev rules
File: $/etc/udev/rules.d/60-arduino-megaAVR.rules
```Bash
# Arduino Mega AVR boards bootloader mode udev rules

# Arduino UNO WiFi Rev2
SUBSYSTEMS=="tty", ENV{ID_REVISION}=="03eb", ENV{ID_MODEL_ID}=="2145", MODE="0666", ENV{ID_MM_DEVICE_IGNORE}="1", ENV{ID_MM_CANDIDATE}="0"
SUBSYSTEMS=="usb", ATTRS{idVendor}=="03eb", ATTRS{idProduct}=="2145", MODE:="0666", ENV{ID_MM_DEVICE_IGNORE}="1"

# Arduino Nano Every
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0058", MODE:="0666"

# Arduino Uno Rev3
SUBSYSTEMS=="usb", ATTRS{idVendor}=="2341", ATTRS{idProduct}=="0043", MODE:="0666"
```

## Bash
```bash
$ sudo make flash
```



## Referências
* [BenMacmorran](https://devblogs.microsoft.com/commandline/connecting-usb-devices-to-wsl/)
* [WSLsupport](https://github.com/dorssel/usbipd-win/wiki/WSL-support#usbip-client-tools)
* [usbipd-win](https://github.com/dorssel/usbipd-win/releases)
* [udev_rules_on_linux](https://support.arduino.cc/hc/en-us/articles/9005041052444-Fix-udev-rules-on-Linux)


